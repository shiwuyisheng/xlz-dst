 **交流群：855460920** 

DST概述

        DST(Distributed scheduling task)是一个基于数据库为注册中心的轻量级的分布式调度框架，由java编写，几乎无依赖第三方jar，核心jar不超过100KB。框架设计旨在为提供一套集功能性、可靠性、易用性、效率性、可维护性、可移植性于一体的高质量分布式调度框架。现已开放全部源代码，并且提供了springboot、spring+mybatis+springmvc、java-application等多种集成方式，后续还会实现python等多种client核心和报警机制。
    DST分为dst-client和dst-console两个部分，两部分无直接关系。dst-client是调度的核心，应用程序集成dst-client并实现加单配置就可以调度托管于dst-console。dst-console提供了应用和调度任务的管理，统计报表等功能，dst-console宕机不影响所有调度的正常运行。

DST特性


应用实例自动注册
     DST应用实例自行注册，并且会保持心跳，自动探测实例存活。注册和管理中心单一，容易维护。

应用实例自动选主（HA）
      DST应用实例会在启动过程中实现自动选主（Master），Master负责任务的分片，保障实例拿到任务的概率一致性。

DST实例的自动分片
     DST根据应用实例有Master实例负责对所有已经激活的调度任务进行分片，分片规则可自行配置。

调度实现简单
     只需要实现定义的接口就可以完成自定义方法的调度和高效调度。

调度中心化管理
     管理平台可以实现所有任务的管理，可以对调度任务实现工台添加和启停，根据需要自动绑定服务器，动态修改调度时间配置不停歇，动态配置处理线程，动态修改调度数据量。

调度规则优化配置
     调度规则支持标准的Cron表达式，也可以制定简单的时间间隔（比如：500s运行一次，只需要配置500）。

动态扩容
     可根据业务容量不停服添加机器，自行获取调度分片，任意加入调度。

动态缩容
     在部分服务宕机或者缩容的情况下，应用会重新进行选主，重新计算分片，对数据处理无阻碍。

调度统计
     在线查看调度趋势图和调度明细，查看每个线程额处理结果和耗时。

架构优越
     系统架构优越，更能节省服务器资源，每个DST实例统一的任务刷新、统计、清理线程，无额外资源损耗。应用基于spring-boot+maven构建。
    

控制台


快速入门


    1、环境准备


    JDK：1.8+
    Mysql：5+
    Maven：3+
    git；2+


    2、下载


    git clone https://gitee.com/xiaoleiziemail/xlz-dst.git
    源码下载后有两部分dst和dst-demo。dst里面是dst的核心代码，dst-demo是各种接入方式
    对dst进行maven编译


    3、应用集成dst-client


    当前以spring+mybatis等工程为例，采用maven进行构建，jdk要求1.7+，当前以dst-demo中dst-ssm为例。
    3.1添加依赖，当前稳定版本为1.0.1
    <dependency>
        <groupId>com.xlz</groupId>
        <artifactId>dst-client</artifactId>
        <version>1.0.1</version>
    </dependency>
    框架在设计之初就考虑了依赖性问题，所以尽量的没有使用到第三方的jar，不过还是用到了一些jar，如果出现jar冲突请自行排除，最好是如下配置：
    <dependency>
        <groupId>com.xlz</groupId>
        <artifactId>dst-client</artifactId>
        <version>1.0.1</version>
        <exclusions>
            <exclusion>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-classic</artifactId>
            </exclusion>
            <exclusion>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
            </exclusion>
            <exclusion>
                <groupId>org.springframework</groupId>
                <artifactId>spring-beans</artifactId>
            </exclusion>
            <exclusion>
                <groupId>org.springframework</groupId>
                <artifactId>spring-core</artifactId>
            </exclusion>
            <exclusion>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
            </exclusion>
            <exclusion>
                <groupId>commons-logging</groupId>
                <artifactId>commons-logging</artifactId>
            </exclusion>
        </exclusions>
    </dependency>

    3.2、应用程序配置方面需要配置调度中心的数据库数据源：
    <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource">
        <property name="driverClassName" value="${jdbc.driverClassName.db2}" />
        <property name="url" value="${jdbc.url.db2}" />
        <property name="username" value="${jdbc.username.db2}" />
        <property name="password" value="${jdbc.password.db2}" />
        <property name="maxActive" value="${jdbc.maxActive.db2}" />  
        <property name="initialSize" value="${jdbc.initialSize.db2}" />  
           <property name="maxWait" value="${jdbc.maxWait.db2}" />  
        <property name="maxIdle" value="${jdbc.maxIdle.db2}" />
        <property name="testOnBorrow" value="${jdbc.testOnBorrow.db2}" />
        <property name="validationQuery" value="select 1 from dual" /> 
        <property name="defaultAutoCommit" value="false"></property>
    </bean>
    
    3.3、引入分布式调度框架
    <bean id="dstManagerBeanFactory" class="com.xlz.manager.DstManagerBeanFactory">
        <property name="dataSource" ref="dataSource"></property>
        <property name="appNo" value="ssm"></property> <!--应用编号-->
    </bean>
    
    3.4、开发调度任务，需要继承AbstractWorker抽象类，里面主要由select方法和execute

    import java.util.Arrays;
    import java.util.List;

    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import org.springframework.beans.factory.annotation.Autowired;

    import com.test.bo.MyTestDO;
    import com.test.service.MyTestService;
    import com.xlz.domain.ShardItem;
    import com.xlz.worker.AbstractWorker;

    public class TestScheduleBean1 extends AbstractWorker<MyTestDO>{
        
        protected final Logger LOG = LoggerFactory.getLogger(getClass());
        @Autowired
        private MyTestService myTestService;
        
        @Override
        public List<MyTestDO> select() {
            ShardItem[] items = getShardItems();
            StringBuffer item = new StringBuffer();
            for(int i =0 ;i < items.length;i++){
                if(i  > 0)
                    item.append(",");
                item.append(items[i].getItemId());
            }
            System.out.println("当前任务："+getAppTask().getId()+"==========当前分片："+item.toString());
            //获取数据加入到待处理单据队列
            String tableName = getAppTask().getTaskParam();
            List<MyTestDO> list = myTestService.
                    getMyTestList(getShardItemCount(),item.toString(),
                            getAppTask().getFetchDataNumber(),tableName);
            /*if("my_test_1".equals(tableName)){
                LOG.error("本次加载到的记录条数为：{},当前任务总分片数为：{}，当前调度获取到的分片为拼接后：{}---原始为{}",list.size(),getShardItemCount(),item.toString(),getShardItems());
            }*/
            return list;
        }

        @Override
        protected void execute(MyTestDO obj) throws Exception{
            String tableName = getAppTask().getTaskParam();
            myTestService.update(obj,tableName,getRegisterWorker());
        }
        
        public void test(){
            LOG.info("============我是自定义方法=============="+Arrays.toString(getShardItems()));
        }

    }


    到此应用配置结束，但此时未完成控制台的配置，调度不能执行。

    4、dst-console安装使用


    4.1、部署
    在mysql中初始化dst和dst_console两个数据库，脚本在dst/sql目录中
    当前dst-console采用springboot开发,jdk1.8+，编译完后可以直接命令行：
    java -jar dst-console.jar
    linux后台启动可以执行命令：
    nohup java -jar dst-console.jar &
    
    也可以打包成war丢进tomact中部署，要求tomcat1.8+
    
    4.2、应用配置
    初始化数据中我们看到有配置的应用数据，当前我们使用的ssm这个测试应用

                            

    4.3、任务配置

    初始化数据中我们看到有配置的任务数据，当前我们使用的是

            

            至此系统已经可以按照指定的调度时间进行调度。

        