package com.xlz.test;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import com.xlz.ds.DataSourceFactory;
import com.xlz.ds.PooledDataSource;
import com.xlz.ds.PooledDataSourceFactory;
import com.xlz.manager.DefaultDstManager;

public class DstApplicationTest {

	public static void main(String[] args) throws IOException {
		DefaultDstManager dstManager = new DefaultDstManager();
		dstManager.getContext().setAppNo("application");
		dstManager.setDataSource(initJdbcUtils());
		dstManager.start();
		System.in.read();
	}

	public static DataSource initJdbcUtils() {
		DataSourceFactory dataSourceFactory = new PooledDataSourceFactory();
		Properties props = new Properties();
		dataSourceFactory.setProperties(props);
		PooledDataSource pooledDataSource = (PooledDataSource) dataSourceFactory.getDataSource();
		pooledDataSource.setDriver("com.mysql.jdbc.Driver");
		pooledDataSource.setUrl("jdbc:mysql://127.0.0.1:3306/dst?useUnicode=true&amp;characterEncoding=UTF-8");
		pooledDataSource.setUsername("test");
		pooledDataSource.setPassword("zhangll"); 
		pooledDataSource.setPoolMaximumActiveConnections(10);
		pooledDataSource.setPoolMaximumCheckoutTime(1000 * 3);
		pooledDataSource.setPoolPingQuery("select 1");
		pooledDataSource.setDefaultAutoCommit(false);
		return pooledDataSource;
	}
}
