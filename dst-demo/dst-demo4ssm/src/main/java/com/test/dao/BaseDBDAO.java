/**
 * 
 */
package com.test.dao;

import com.test.bo.BaseDomain;

/**
 * @author zhangll
 *
 */
public interface BaseDBDAO<T extends BaseDomain> {

    Integer save(T t);

    int update(T t);

    void delete(Integer id);

    T findById(Integer id);

//    List<T> findByPage(@Param("filterRules") List<FilterRule> filterRules, @Param("pageQuery") PageQuery pageQuery);
//
//    Integer getTotalCount(@Param("filterRules") List<FilterRule> filterRules);
}
