package com.test.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.test.bo.MyTestDO;

public abstract interface MyTestDBDAO extends BaseDBDAO<MyTestDO>
{
  public abstract List<MyTestDO> getMyTestList(@Param("len") Integer len,
		  @Param("item") String item,@Param("fetchDataNumber") Long fetchDataNumber,
		  @Param("tableName")String tableName);
  
  public abstract int update(@Param("id")Integer id,@Param("tableName")String tableName,@Param("instance")String instance);
}