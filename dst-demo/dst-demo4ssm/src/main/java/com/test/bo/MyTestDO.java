package com.test.bo;

public class MyTestDO extends BaseDomain {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8084506546860338803L;
	private String orderNo;
	private Integer status;

	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
