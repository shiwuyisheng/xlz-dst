package com.test.service;

import java.util.List;

import com.test.bo.BaseDomain;

public interface BaseService<T extends BaseDomain> {
    /**
     * 根据id找到对应的DO对象
     * 
     * @param id
     * @return
     */
    T findById(Integer id);

    /**
     * 更新数据
     * 
     * @param t
     */
    void update(T t);

    /**
     * 创建新纪录,返回对应的id
     * 
     * @param entity
     * @return
     */
    Integer save(T entity);

    /**
     * 根据id删除记录
     * 
     * @param ids 多个id以半角英文逗号分隔 如;1,2,3
     */
    void delete(String ids);

    /**
     * 查找指定的记录列表
     * 
     * @param filterRuleList 需要过滤的查询条件
     * @param pageQuery
     * @return
     */
    //List<T> findByPage(List<FilterRule> filterRuleList, PageQuery pageQuery);

    /**
     * 查找指定的记录列表的总数
     * 
     * @param filterRuleList
     * @return
     */
    //Integer getTotalCount(List<FilterRule> filterRuleList);

}
