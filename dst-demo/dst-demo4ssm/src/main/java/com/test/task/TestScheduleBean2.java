package com.test.task;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.test.bo.MyTestDO;
import com.test.service.MyTestService;
import com.xlz.domain.ShardItem;
import com.xlz.worker.AbstractWorker;

public class TestScheduleBean2 extends AbstractWorker<MyTestDO>{
	
	protected final Logger LOG = LoggerFactory.getLogger(getClass());
	@Autowired
	private MyTestService myTestService;
	
	@Override
	public List<MyTestDO> select() {
		ShardItem[] items = getShardItems();
		
		StringBuffer item = new StringBuffer();
		for(int i =0 ;i < items.length;i++){
			if(i  > 0)
				item.append(",");
			item.append(items[i].getItemId());
		}
		//System.out.println("==========当前分片："+item.toString());
		//获取数据加入到待处理单据队列
		String tableName = getAppTask().getTaskParam();
		List<MyTestDO> list = myTestService.
				getMyTestList(getShardItemCount(),item.toString(),
						getAppTask().getFetchDataNumber(),tableName);
		return list;
	}

	@Override
	protected void execute(MyTestDO obj) throws Exception{

		String tableName = getAppTask().getTaskParam();
		myTestService.update(obj,tableName,getRegisterWorker());
		
	}
	
	public void test(){
		LOG.info("============我是自定义方法=============="+Arrays.toString(getShardItems()));
	}

}
