package com.xlz.system.service;

import com.xlz.common.domain.Tree;
import com.xlz.system.domain.DeptDO;

import java.util.List;
import java.util.Map;

/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public interface DeptService {
	
	DeptDO get(Long deptId);
	
	List<DeptDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(DeptDO sysDept);
	
	int update(DeptDO sysDept);
	
	int remove(Long deptId);
	
	int batchRemove(Long[] deptIds);

	Tree<DeptDO> getTree();
	
	Tree<DeptDO> getTree(Long deptId);
	
	String getAllChild(Long deptId);
	
	boolean checkDeptHasUser(Long deptId);
}
