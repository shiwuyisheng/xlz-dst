package com.xlz.schedule.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlz.common.controller.BaseController;
import com.xlz.common.datasource.DatabaseContextHolder;
import com.xlz.common.datasource.DatabaseType;
import com.xlz.common.utils.PageUtils;
import com.xlz.common.utils.Query;
import com.xlz.schedule.domain.StatisticExecute;
import com.xlz.schedule.service.StatisticExecuteService;

/**
 * Created by zhangleilei 
 */
@Controller
@RequestMapping("/schedule/execute")
public class StatisticExecuteController extends BaseController {
	String prefix = "schedule/execute";
	
    @Resource
    private StatisticExecuteService statisticExecuteService;

    @GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		// 查询列表数据
		Query query = new Query(params);
		query.put("sort", "id");
		query.put("order", "desc");
		DatabaseContextHolder.setDatabaseType(DatabaseType.schedule);
		List<StatisticExecute> list = statisticExecuteService.list(query);
		int total = statisticExecuteService.count(query);
		PageUtils pageUtil = new PageUtils(list, total);
		return pageUtil;
	}

}
