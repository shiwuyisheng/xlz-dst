package com.xlz.schedule.domain;

import java.util.Date;

import com.xlz.common.domain.BaseDO;
/**
 * @author ������
 */
public class AppWorker extends BaseDO{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 任务id */
	private String taskId;  
	/** 注册的实例id */
	private String registerInstanceId;  
	/** 注册的任务组 */
	private String registerWorker;  
	/** 任务项 */
	private String shardItem;  
	/** 上次加载数据时间 */
	private String loadTime;  
	/** 处理数据时间（最近一次心跳上传） */
	private Date executeTime;  
	/** 加载的记录总条数 */
	private String loadTotal;  
	/** 执行的记录总条数（最近一次心跳上传） */
	private String executeTotal;  
	/** 单任务组待处理任务记录数 */
	private String waitDealTotal;  
	/** 应用编号 */
	private String appNo;  
  	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getRegisterInstanceId() {
		return registerInstanceId;
	}

	public void setRegisterInstanceId(String registerInstanceId) {
		this.registerInstanceId = registerInstanceId;
	}
	public String getRegisterWorker() {
		return registerWorker;
	}

	public void setRegisterWorker(String registerWorker) {
		this.registerWorker = registerWorker;
	}
	public String getShardItem() {
		return shardItem;
	}

	public void setShardItem(String shardItem) {
		this.shardItem = shardItem;
	}
	public String getLoadTime() {
		return loadTime;
	}

	public void setLoadTime(String loadTime) {
		this.loadTime = loadTime;
	}
	public Date getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(Date executeTime) {
		this.executeTime = executeTime;
	}
	public String getLoadTotal() {
		return loadTotal;
	}

	public void setLoadTotal(String loadTotal) {
		this.loadTotal = loadTotal;
	}
	public String getExecuteTotal() {
		return executeTotal;
	}

	public void setExecuteTotal(String executeTotal) {
		this.executeTotal = executeTotal;
	}
	public String getWaitDealTotal() {
		return waitDealTotal;
	}

	public void setWaitDealTotal(String waitDealTotal) {
		this.waitDealTotal = waitDealTotal;
	}
	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}

	@Override
	public String toString() {
		return "AppWorker [taskId=" + taskId + ", registerInstanceId=" + registerInstanceId + ", registerWorker="
				+ registerWorker + ", shardItem=" + shardItem + ", loadTime=" + loadTime + ", executeTime="
				+ executeTime + ", loadTotal=" + loadTotal + ", executeTotal=" + executeTotal + ", waitDealTotal="
				+ waitDealTotal + ", appNo=" + appNo + "]";
	}
    
}