package com.xlz.schedule.service;

import java.util.List;
import java.util.Map;

import com.xlz.common.service.BaseService;
import com.xlz.common.utils.Query;
import com.xlz.schedule.domain.StatisticExecute;

/**
 * Created by zhangll
 */
public interface StatisticExecuteService extends BaseService<StatisticExecute>{

	List<Map> executeDataTotal(Query query);
	
	List<Map> executeDataTime(Query query);
	
}
