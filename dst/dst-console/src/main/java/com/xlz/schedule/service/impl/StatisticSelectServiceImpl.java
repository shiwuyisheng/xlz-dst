package com.xlz.schedule.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlz.common.dao.BaseDao;
import com.xlz.common.service.impl.BaseServiceImpl;
import com.xlz.common.utils.Query;
import com.xlz.schedule.dao.StatisticSelectDao;
import com.xlz.schedule.domain.StatisticSelect;
import com.xlz.schedule.service.StatisticSelectService;

/**
 * Created by zhangleilei
 */
@Service
public class StatisticSelectServiceImpl extends BaseServiceImpl<StatisticSelect> implements StatisticSelectService{

	@Autowired
	private StatisticSelectDao statisticSelectDao;
	
	@Override
	protected BaseDao<StatisticSelect> getDAO() {
		return statisticSelectDao;
	}

	@Override
	public List<Map> selectDataTotal(Query query) {
		return statisticSelectDao.selectDataTotal(query);
	}
	
	@Override
	public List<Map> selectDataTime(Query query) {
		return statisticSelectDao.selectDataTime(query);
	}

}
