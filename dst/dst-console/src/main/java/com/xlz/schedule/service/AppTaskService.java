package com.xlz.schedule.service;

import java.util.List;
import java.util.Map;

import com.xlz.common.service.BaseService;
import com.xlz.schedule.domain.AppTask;

/**
 * Created by zhangll
 */
public interface AppTaskService extends BaseService<AppTask>{

	List<Map<String, Object>> findAppTaskChartData();
}
