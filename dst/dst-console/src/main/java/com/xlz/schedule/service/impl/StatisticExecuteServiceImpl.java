package com.xlz.schedule.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlz.common.dao.BaseDao;
import com.xlz.common.service.impl.BaseServiceImpl;
import com.xlz.common.utils.Query;
import com.xlz.schedule.dao.StatisticExecuteDao;
import com.xlz.schedule.domain.StatisticExecute;
import com.xlz.schedule.service.StatisticExecuteService;

/**
 * Created by zhangleilei
 */
@Service
public class StatisticExecuteServiceImpl extends BaseServiceImpl<StatisticExecute> implements StatisticExecuteService{

	@Autowired
	private StatisticExecuteDao statisticExecuteDao;
	
	@Override
	protected BaseDao<StatisticExecute> getDAO() {
		return statisticExecuteDao;
	}
	
	@Override
	public List<Map> executeDataTotal(Query query) {
		return statisticExecuteDao.executeDataTotal(query);
	}
	
	@Override
	public List<Map> executeDataTime(Query query) {
		return statisticExecuteDao.executeDataTime(query);
	}
}
