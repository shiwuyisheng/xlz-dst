package com.xlz.schedule.service;

import java.util.List;
import java.util.Map;

import com.xlz.common.service.BaseService;
import com.xlz.common.utils.Query;
import com.xlz.schedule.domain.StatisticSelect;

/**
 * Created by zhangll
 */
public interface StatisticSelectService extends BaseService<StatisticSelect>{

	List<Map> selectDataTotal(Query query);
	
	List<Map> selectDataTime(Query query);


}
