package com.xlz.schedule.domain;

import com.xlz.common.domain.BaseDO;
/**
 * @author ������
 */
public class AppTask extends BaseDO{
    private static final long serialVersionUID = -8736616045315083846L;

    private String taskName               ;
    private String dealBean               ;
    private String cronExpression         ;
    private String taskParam              ;
    private Long fetchDataNumber       ;
    private Integer taskGroupCount        ;
    private Integer singleTaskThreadCount;
    private String shardItem              ;
    private String bindIp                 ;
    private Long taskBacklogData       ;
    private String appNo                  ;
    private Integer readiness               ;
    private String executeMethod          ;
    private Long deadTime       ;
    
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getDealBean() {
		return dealBean;
	}

	public void setDealBean(String dealBean) {
		this.dealBean = dealBean;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getTaskParam() {
		return taskParam;
	}

	public void setTaskParam(String taskParam) {
		this.taskParam = taskParam;
	}

	public Long getFetchDataNumber() {
		return fetchDataNumber;
	}

	public void setFetchDataNumber(Long fetchDataNumber) {
		this.fetchDataNumber = fetchDataNumber;
	}

	public Integer getTaskGroupCount() {
		return taskGroupCount;
	}

	public void setTaskGroupCount(Integer taskGroupCount) {
		this.taskGroupCount = taskGroupCount;
	}

	public Integer getSingleTaskThreadCount() {
		return singleTaskThreadCount;
	}

	public void setSingleTaskThreadCount(Integer singleTaskThreadCount) {
		this.singleTaskThreadCount = singleTaskThreadCount;
	}

	public String getShardItem() {
		return shardItem;
	}

	public void setShardItem(String shardItem) {
		this.shardItem = shardItem;
	}

	public String getBindIp() {
		return bindIp;
	}

	public void setBindIp(String bindIp) {
		this.bindIp = bindIp;
	}


	public Long getTaskBacklogData() {
		return taskBacklogData;
	}

	public void setTaskBacklogData(Long taskBacklogData) {
		this.taskBacklogData = taskBacklogData;
	}

	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}

	public Integer getReadiness() {
		return readiness;
	}

	public void setReadiness(Integer readiness) {
		this.readiness = readiness;
	}

	public String getExecuteMethod() {
		return executeMethod;
	}

	public void setExecuteMethod(String executeMethod) {
		this.executeMethod = executeMethod;
	}

	public Long getDeadTime() {
		return deadTime;
	}

	public void setDeadTime(Long deadTime) {
		this.deadTime = deadTime;
	}

	@Override
	public String toString() {
		return "ScheduleTask [taskName=" + taskName + ", dealBean=" + dealBean + ", cronExpression=" + cronExpression
				+ ", taskParam=" + taskParam + ", fetchDataNumber=" + fetchDataNumber + ", taskGroupCount="
				+ taskGroupCount + ", singleTaskThreadCount=" + singleTaskThreadCount + ", shardItem=" + shardItem
				+ ", bindIp=" + bindIp + ", taskBacklogData=" + taskBacklogData + ", appNo=" + appNo + ", readiness="
				+ readiness + ", executeMethod=" + executeMethod + ", deadTime=" + deadTime
				+ "]";
	}

}