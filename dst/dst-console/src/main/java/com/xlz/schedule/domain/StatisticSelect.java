package com.xlz.schedule.domain;

import com.xlz.common.domain.BaseDO;

public class StatisticSelect extends BaseDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 应用编号 */
	private String appNo;  
	/** 任务id */
	private String taskId;  
	/** 注册的实例id */
	private String registerInstanceId;  
	/** 注册的任务组 */
	private String registerWorker;  
	/** 加载数据耗时 */
	private String loadDataTime;  
	/** 加载的记录总条数 */
	private String loadDataTotal;  
	/** 0成功，1失败 */
	private Integer loadDataFlag;  
	/** 异常信息 */
	private String errorContent;  
  	
	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getRegisterInstanceId() {
		return registerInstanceId;
	}

	public void setRegisterInstanceId(String registerInstanceId) {
		this.registerInstanceId = registerInstanceId;
	}
	public String getRegisterWorker() {
		return registerWorker;
	}

	public void setRegisterWorker(String registerWorker) {
		this.registerWorker = registerWorker;
	}

	public String getLoadDataTime() {
		return loadDataTime;
	}

	public void setLoadDataTime(String loadDataTime) {
		this.loadDataTime = loadDataTime;
	}
	public String getLoadDataTotal() {
		return loadDataTotal;
	}

	public void setLoadDataTotal(String loadDataTotal) {
		this.loadDataTotal = loadDataTotal;
	}
	public Integer getLoadDataFlag() {
		return loadDataFlag;
	}

	public void setLoadDataFlag(Integer loadDataFlag) {
		this.loadDataFlag = loadDataFlag;
	}
	public String getErrorContent() {
		return errorContent;
	}

	public void setErrorContent(String errorContent) {
		this.errorContent = errorContent;
	}

}