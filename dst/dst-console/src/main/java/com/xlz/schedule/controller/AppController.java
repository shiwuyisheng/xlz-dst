package com.xlz.schedule.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlz.common.annotation.Log;
import com.xlz.common.controller.BaseController;
import com.xlz.common.datasource.DatabaseContextHolder;
import com.xlz.common.datasource.DatabaseType;
import com.xlz.common.utils.PageUtils;
import com.xlz.common.utils.Query;
import com.xlz.common.utils.R;
import com.xlz.schedule.domain.App;
import com.xlz.schedule.service.AppService;
import com.xlz.system.domain.DeptDO;
import com.xlz.system.service.DeptService;

/**
 * Created by zhangleilei 
 */
@Controller
@RequestMapping("/schedule/app")
public class AppController extends BaseController {
	String prefix = "schedule/app";
	
    @Resource
    private AppService appService;
    @Resource
    private DeptService deptService;

    @RequiresPermissions("schedule:app:main")
	@GetMapping()
	String role() {
		return prefix + "/main";
	}

    @GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		// 查询列表数据
		Query query = new Query(params);
		List<App> list = appService.list(query);
		int total = appService.count(query);
		PageUtils pageUtil = new PageUtils(list, total);
		return pageUtil;
	}

	@Log("添加任务")
	@RequiresPermissions("schedule:app:add")
	@GetMapping("/add")
	String add() {
		return prefix + "/add";
	}

	@Log("编辑任务")
	@RequiresPermissions("schedule:app:edit")
	@GetMapping("/edit/{id}")
	String edit(@PathVariable("id") Long id, Model model) {
		App entity = appService.get(id);
		//获取部门名称
		DatabaseContextHolder.setDatabaseType(DatabaseType.system);
		DeptDO deptDO = deptService.get(entity.getDeptId());
		if(deptDO != null)
			entity.setDeptName(deptDO.getName());
		model.addAttribute("entity", entity);
		return prefix + "/edit";
	}

	@Log("保存任务")
	@RequiresPermissions("schedule:app:add")
	@PostMapping("/save")
	@ResponseBody()
	R save(App entity) {
		if(entity.getDeptId() == null){
			entity.setDeptId(getUser().getDeptId());
		}
		entity.setCreateUser(getUsername());
		if (appService.save(entity) > 0) {
			return R.ok();
		} else {
			return R.error(1, "保存失败");
		}
	}

	@Log("更新任务")
	@RequiresPermissions("schedule:app:edit")
	@PostMapping("/update")
	@ResponseBody()
	R update(App entity) {
		entity.setUpdateUser(getUsername());
		if (appService.update(entity) > 0) {
			return R.ok();
		} else {
			return R.error(1, "保存失败");
		}
	}

	@Log("删除任务")
	@RequiresPermissions("schedule:app:remove")
	@PostMapping("/remove")
	@ResponseBody()
	R remove(Long id) {
		if (appService.remove(id) > 0) {
			return R.ok();
		} else {
			return R.error(1, "删除失败");
		}
	}
	
	@RequiresPermissions("schedule:app:batchRemove")
	@Log("批量删除任务")
	@PostMapping("/batchRemove")
	@ResponseBody
	R batchRemove(@RequestParam("ids[]") Long[] ids) {
		int r = appService.batchRemove(ids);
		if (r > 0) {
			return R.ok();
		}
		return R.error();
	}

}
