package com.xlz.schedule.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlz.common.controller.BaseController;
import com.xlz.common.datasource.DatabaseContextHolder;
import com.xlz.common.datasource.DatabaseType;
import com.xlz.common.utils.PageUtils;
import com.xlz.common.utils.Query;
import com.xlz.schedule.domain.AppInstance;
import com.xlz.schedule.service.AppInstanceService;

/**
 * Created by zhangleilei 
 */
@Controller
@RequestMapping("/schedule/instance")
public class AppInstanceController extends BaseController {
	String prefix = "schedule/instance";
	
    @Resource
    private AppInstanceService appInstanceService;

    @RequiresPermissions("schedule:instance:main")
	@GetMapping()
	String role() {
		return prefix + "/main";
	}

    @GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		// 查询列表数据
		Query query = new Query(params);
		DatabaseContextHolder.setDatabaseType(DatabaseType.schedule);
		List<AppInstance> list = appInstanceService.list(query);
		int total = appInstanceService.count(query);
		PageUtils pageUtil = new PageUtils(list, total);
		return pageUtil;
	}

}
