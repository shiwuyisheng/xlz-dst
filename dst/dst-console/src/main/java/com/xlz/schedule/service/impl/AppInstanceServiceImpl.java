package com.xlz.schedule.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlz.common.dao.BaseDao;
import com.xlz.common.service.impl.BaseServiceImpl;
import com.xlz.schedule.dao.AppInstanceDao;
import com.xlz.schedule.domain.AppInstance;
import com.xlz.schedule.service.AppInstanceService;

/**
 * Created by zhangleilei
 */
@Service
public class AppInstanceServiceImpl extends BaseServiceImpl<AppInstance> implements AppInstanceService{

	@Autowired
	private AppInstanceDao scheduleAppInstanceDao;
	
	@Override
	protected BaseDao<AppInstance> getDAO() {
		return scheduleAppInstanceDao;
	}

}
