package com.xlz.common.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
@ConditionalOnProperty(prefix = "dst", name = "spring-session-open", havingValue = "true")
public class SpringSessionConfig {

}
