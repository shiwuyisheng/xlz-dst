package com.xlz.common.service;

import java.util.List;
import java.util.Map;

import com.xlz.common.domain.BaseDO;
/**
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public interface BaseService<T extends BaseDO> {
	
	/**
     * 根据id找到对应的DO对象
     * 
     * @param id
     * @return
     */
    T get(Long id);

	List<T> list(Map<String, Object> map);

	int count(Map<String, Object> map);

	int save(T t);

	int update(T t);

	int remove(Long tId);

	int batchRemove(Long[] ids);
}
