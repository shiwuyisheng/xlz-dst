var deptId;
$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});
function save() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/schedule/app/save",
		data : $('#signupForm').serialize(), // 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("网络超时");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name);
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			appNo : {
				required : true
			},
			appName : {
				required : true
			},
			heartBeatRate : {
				required : true,
				number : true,
				min : 3,
				max : 20
			},
			deadHeartCount : {
				required : true,
				number : true,
				min : 20
			},
			deptName : {
				required : true
			}
		},
		messages : {
			appNo : {
				required : icon + "请输入应用编号"
			},
			appName : {
				required : icon + "请输入应用名称"
			},
			heartBeatRate : {
				required : icon + "请输入心跳时间",
				number : icon + "输入只能为数字",
				min : "最小为3 s",
				max : "最大为20 s"
			},
			deadHeartCount : {
				required : icon + "请输入超过几个心跳死亡",
				number : icon + "输入只能为数字",
				min : "最小为20 "
			},
			deptName : {
				required : icon + "请选择所属部门"
			}
		}
	})
}

var openDept = function(){
	layer.open({
		type:2,
		title:"选择部门",
		area : [ '300px', '450px' ],
		content:"/system/sysDept/treeView"
	})
}
function loadDept( deptId,deptName){
	$("#deptId").val(deptId);
	$("#deptName").val(deptName);
}