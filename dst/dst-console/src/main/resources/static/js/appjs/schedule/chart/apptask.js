var prefix = "/schedule/chart"
$(function() {
	option = {
	    title : {
	        text: '所有调度在应用中分布图',
	        //subtext: '',
	        x:'center'
	    },
	    tooltip : {
	        trigger: 'item',
	        formatter: "{a} <br/>{b} : {c} ({d}%)"
	    },
	    legend: {
	        type: 'scroll',
	        orient: 'vertical',
	        right: 10,
	        top: 20,
	        bottom: 20,
	        data: [],

	        selected: []
	    },
	    series : [
	        {
	            name: '姓名',
	            type: 'pie',
	            radius : '55%',
	            center: ['40%', '50%'],
	            data: [],
	            itemStyle: {
	                emphasis: {
	                    shadowBlur: 10,
	                    shadowOffsetX: 0,
	                    shadowColor: 'rgba(0, 0, 0, 0.5)'
	                }
	            }
	        }
	    ]
	};

	var myChart = echarts.init(document.getElementById('echarts-pie-chart'),"vintage");
	myChart.setOption(option);
	myChart.showLoading();
	
     var legendData = [];
     var seriesData = [];
     var selected = {};
	    
     $.ajax({
	     type : "get",
	     async : true,            //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
	     url : prefix + "/apptaskData",    //请求发送到TestServlet处
	     data : {},
	     dataType : "json",        //返回数据形式为json
	     success : function(result) {
	         //请求成功时执行该函数内容，result即为服务器返回的json对象
	         if (result) {
	                myChart.hideLoading();    //隐藏加载动画
	                myChart.setOption({        //加载数据图表
	                	legend: {
	                        data: result.legendData,
	                        selected:selected
	                    },
	                    series: [{
	                        // 根据名字对应到相应的系列
	                        data: result.seriesData
	                    }]
	                });
	         }
	    },
	     error : function(errorMsg) {
	         //请求失败时执行该函数
		     alert("图表请求数据失败!");
		     myChart.hideLoading();
	     }
	})
});
