﻿/*
SQLyog Community Edition- MySQL GUI v6.04
Host - 5.6.21-log : Database - dst
*********************************************************************
Server version : 5.6.21-log
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

create database if not exists `dst`;

USE `dst`;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `app` */

DROP TABLE IF EXISTS `app`;

CREATE TABLE `app` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '应用主键',
  `app_no` varchar(50) NOT NULL COMMENT '应用编号',
  `app_name` varchar(100) NOT NULL COMMENT '应用名',
  `heart_beat_rate` bigint(11) NOT NULL COMMENT '心跳时间',
  `active` int(11) DEFAULT NULL COMMENT '可用状态',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(100) DEFAULT NULL COMMENT '更新人',
  `dead_heart_count` int(11) DEFAULT '5' COMMENT '死亡心跳个数，最少5个',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_no` (`app_no`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `app` */

insert  into `app`(`id`,`app_no`,`app_name`,`heart_beat_rate`,`active`,`create_time`,`create_user`,`update_time`,`update_user`,`dead_heart_count`,`dept_id`) values (7,'springboot','springboot测试',3,1,'2018-03-27 17:21:17','admin','2018-03-27 17:21:55','admin',20,8),(8,'application','application测试',5,1,'2018-03-27 19:30:13','admin','2018-03-27 19:30:24','admin',20,8),(9,'ssm','ssm测试',3,1,'2018-03-27 19:37:57','admin',NULL,NULL,20,8);

/*Table structure for table `app_instance` */

DROP TABLE IF EXISTS `app_instance`;

CREATE TABLE `app_instance` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '实例id',
  `app_no` varchar(50) NOT NULL COMMENT '应用标识',
  `register_instance_id` varchar(100) DEFAULT NULL COMMENT '注册的实例id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `register_instance_ip` (`app_no`,`register_instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=597 DEFAULT CHARSET=utf8;

/*Data for the table `app_instance` */

insert  into `app_instance`(`id`,`app_no`,`register_instance_id`,`create_time`,`update_time`) values (589,'test_app','192.168.56.1$ef38c432-e037-4398-95eb-6d10426d46eb','2018-03-27 17:31:41','2018-03-27 17:31:41'),(595,'springboot','192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e','2018-03-27 19:17:41','2018-03-27 19:17:50'),(596,'application','192.168.56.1$f1c16c4e-a0c8-40c1-870b-af81c7eaf2ea','2018-03-27 19:33:17','2018-03-27 19:33:27');

/*Table structure for table `app_task` */

DROP TABLE IF EXISTS `app_task`;

CREATE TABLE `app_task` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '任务主键',
  `task_name` varchar(100) NOT NULL COMMENT '任务名',
  `deal_bean` varchar(100) NOT NULL COMMENT '处理bean',
  `cron_expression` varchar(20) NOT NULL COMMENT '执行表达式',
  `task_param` varchar(255) DEFAULT NULL COMMENT '任务参数',
  `fetch_data_number` bigint(11) DEFAULT NULL COMMENT '每次抓取数据的条数',
  `task_group_count` int(11) DEFAULT NULL COMMENT '任务组（单jvm的任务组），默认为0',
  `single_task_thread_count` int(11) DEFAULT NULL COMMENT '单个任务的执行线程数',
  `shard_item` text COMMENT '分布式任务项',
  `bind_ip` varchar(500) DEFAULT '127.0.0.1' COMMENT '绑定ip',
  `active` int(11) DEFAULT NULL COMMENT '可用状态',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(100) DEFAULT NULL COMMENT '更新人',
  `task_backlog_data` bigint(11) DEFAULT NULL COMMENT '数据积压',
  `app_no` varchar(50) DEFAULT NULL COMMENT '应用编号',
  `readiness` int(11) DEFAULT NULL COMMENT '准备就绪（0否，1是）',
  `execute_method` varchar(100) DEFAULT NULL COMMENT '自定义执行方法',
  `dead_time` bigint(20) NOT NULL DEFAULT '20000' COMMENT '假死时长，超过这个时间认为已经死掉了',
  PRIMARY KEY (`id`),
  KEY `app_no` (`app_no`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `app_task` */

insert  into `app_task`(`id`,`task_name`,`deal_bean`,`cron_expression`,`task_param`,`fetch_data_number`,`task_group_count`,`single_task_thread_count`,`shard_item`,`bind_ip`,`active`,`create_time`,`create_user`,`update_time`,`update_user`,`task_backlog_data`,`app_no`,`readiness`,`execute_method`,`dead_time`) values (21,'my_test_1','testScheduleBean1','0/1 * * * * ?','test',500,1,3,'0#name=zhangsan|1#name=zhangsan|2#name=zhangsan|3#name=zhangsan|4#name=zhangsan|5#name=zhangsan','127.0.0.1',1,'2018-03-27 17:29:32','admin','2018-03-27 19:17:47','admin',0,'springboot',1,'',20),(22,'my_test_2','com.xlz.test.TestScheduleBean1','0/3 * * * * ?','my_test_1',500,1,1,'0#name=zhangsan|1#name=zhangsan|2#name=zhangsan|3#name=zhangsan|4#name=zhangsan|5#name=zhangsan','127.0.0.1',1,'2018-03-27 19:31:55','admin','2018-03-27 19:33:20','admin',0,'application',1,'myTest',20),(23,'my_test_4','testScheduleBean1','0/3 * * * * ?','my_test_4',500,3,3,'0#name=zhangsan|1#name=zhangsan|2#name=zhangsan|3#name=zhangsan|4#name=zhangsan|5#name=zhangsan','127.0.0.1',0,'2018-03-27 19:38:58','admin',NULL,NULL,0,'ssm',1,'',20),(24,'my_test_3','testScheduleBean1','0/1 * * * * ?','my_test_3',100,3,3,'0#name=zhangsan|1#name=zhangsan|2#name=zhangsan|3#name=zhangsan|4#name=zhangsan|5#name=zhangsan','127.0.0.1',0,'2018-03-27 19:39:34','admin',NULL,NULL,0,'ssm',1,'',20),(25,'my_test_5','testScheduleBean2','100','my_test_5',1000,1,3,'0#name=zhangsan|1#name=zhangsan|2#name=zhangsan|3#name=zhangsan|4#name=zhangsan|5#name=zhangsan','127.0.0.1',0,'2018-03-27 19:40:15','admin',NULL,NULL,0,'ssm',1,'',20);

/*Table structure for table `app_worker` */

DROP TABLE IF EXISTS `app_worker`;

CREATE TABLE `app_worker` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '处理实例id',
  `task_id` bigint(11) NOT NULL COMMENT '任务id',
  `register_instance_id` varchar(100) DEFAULT NULL COMMENT '注册的实例id',
  `register_worker` varchar(100) DEFAULT NULL COMMENT '注册的任务组',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `shard_item` varchar(500) NOT NULL DEFAULT 'no_shard' COMMENT '任务项',
  `load_time` bigint(20) DEFAULT NULL COMMENT '上次加载数据时间',
  `execute_time` datetime DEFAULT NULL COMMENT '处理数据时间（最近一次心跳上传）',
  `load_total` bigint(11) DEFAULT '0' COMMENT '加载的记录总条数',
  `execute_total` bigint(11) DEFAULT '0' COMMENT '执行的记录总条数（最近一次心跳上传）',
  `wait_deal_total` bigint(11) DEFAULT '0' COMMENT '单任务组待处理任务记录数',
  `app_no` varchar(50) DEFAULT NULL COMMENT '应用编号',
  `active` int(11) DEFAULT NULL COMMENT '是否可用(0否，1是)',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_no` (`task_id`,`register_instance_id`,`app_no`,`register_worker`)
) ENGINE=InnoDB AUTO_INCREMENT=23380 DEFAULT CHARSET=utf8;

/*Data for the table `app_worker` */

insert  into `app_worker`(`id`,`task_id`,`register_instance_id`,`register_worker`,`create_time`,`shard_item`,`load_time`,`execute_time`,`load_total`,`execute_total`,`wait_deal_total`,`app_no`,`active`,`update_time`) values (23378,21,'192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e','springboot$192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e$21_0','2018-03-27 19:17:47','0#name=zhangsan|1#name=zhangsan|2#name=zhangsan|3#name=zhangsan|4#name=zhangsan|5#name=zhangsan',NULL,NULL,0,0,0,'springboot',1,'2018-03-27 19:17:50'),(23379,22,'192.168.56.1$f1c16c4e-a0c8-40c1-870b-af81c7eaf2ea','application$192.168.56.1$f1c16c4e-a0c8-40c1-870b-af81c7eaf2ea$22_0','2018-03-27 19:33:20','0#name=zhangsan|1#name=zhangsan|2#name=zhangsan|3#name=zhangsan|4#name=zhangsan|5#name=zhangsan',NULL,NULL,0,0,0,'application',1,'2018-03-27 19:33:27');

/*Table structure for table `statistic_execute` */

DROP TABLE IF EXISTS `statistic_execute`;

CREATE TABLE `statistic_execute` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `app_no` varchar(50) DEFAULT NULL COMMENT '应用编号',
  `task_id` bigint(11) NOT NULL COMMENT '任务id',
  `register_instance_id` varchar(100) DEFAULT NULL COMMENT '注册的实例id',
  `register_worker` varchar(100) DEFAULT NULL COMMENT '注册的任务组',
  `execute_data_time` bigint(20) DEFAULT NULL COMMENT '处理数据时间（最近一次心跳上传）',
  `execute_success_total` bigint(11) DEFAULT '0' COMMENT '执行成功的记录总条数',
  `execute_fail_total` bigint(11) DEFAULT '0' COMMENT '执行失败的记录总条数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `statistic_execute` */

insert  into `statistic_execute`(`id`,`app_no`,`task_id`,`register_instance_id`,`register_worker`,`execute_data_time`,`execute_success_total`,`execute_fail_total`,`create_time`) values (1,'springboot',21,'192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e','springboot$192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e$21_0',5730,12,0,'2018-03-27 19:17:51'),(2,'application',22,'192.168.56.1$f1c16c4e-a0c8-40c1-870b-af81c7eaf2ea','application$192.168.56.1$f1c16c4e-a0c8-40c1-870b-af81c7eaf2ea$22_0',0,0,0,'2018-03-27 19:33:27');

/*Table structure for table `statistic_select` */

DROP TABLE IF EXISTS `statistic_select`;

CREATE TABLE `statistic_select` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `app_no` varchar(50) DEFAULT NULL COMMENT '应用编号',
  `task_id` bigint(11) NOT NULL COMMENT '任务id',
  `register_instance_id` varchar(100) DEFAULT NULL COMMENT '注册的实例id',
  `register_worker` varchar(100) DEFAULT NULL COMMENT '注册的任务组',
  `load_data_time` bigint(20) DEFAULT NULL COMMENT '加载数据耗时',
  `load_data_total` bigint(11) DEFAULT '0' COMMENT '加载的记录总条数',
  `load_data_flag` int(11) DEFAULT '0' COMMENT '0成功，1失败',
  `error_content` text COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `app_no` (`app_no`,`task_id`,`register_instance_id`,`register_worker`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `statistic_select` */

insert  into `statistic_select`(`id`,`app_no`,`task_id`,`register_instance_id`,`register_worker`,`load_data_time`,`load_data_total`,`load_data_flag`,`error_content`,`create_time`) values (1,'springboot',21,'192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e','springboot$192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e$21_0',0,3,1,'','2018-03-27 19:17:51'),(2,'springboot',21,'192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e','springboot$192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e$21_0',0,3,1,'','2018-03-27 19:17:51'),(3,'springboot',21,'192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e','springboot$192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e$21_0',0,3,1,'','2018-03-27 19:17:51'),(4,'springboot',21,'192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e','springboot$192.168.56.1$632e9659-08bc-4f10-9065-6e57f29ab89e$21_0',0,3,1,'','2018-03-27 19:17:51');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
