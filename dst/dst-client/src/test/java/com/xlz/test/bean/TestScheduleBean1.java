package com.xlz.test.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xlz.worker.AbstractWorker;

public class TestScheduleBean1 extends AbstractWorker<String>{
	
	protected final Logger LOG = LoggerFactory.getLogger(getClass());
	
	@Override
	public List<String> select() {
		//
		LOG.info("============我来load待处理数据=============="+Arrays.toString(getShardItems()));
		//获取数据加入到待处理单据队列
		List<String> list = new ArrayList<>();
		list.add("task1");
		list.add("task2");
		list.add("task3");
		return list;
	}

	@Override
	protected void execute(String obj) throws Exception{
		//加锁，防止被不同任务实例所处理
		LOG.info("============我来处理数据=============="+obj);
		Thread.sleep((long) (1000*Math.random()));
		
	}
	
	public void test(){
		LOG.info("============我是自定义方法=============="+Arrays.toString(getShardItems()));
		
		
	}

}
