package com.xlz.test;

import java.util.Date;
import java.util.concurrent.CountDownLatch;

import com.xlz.util.CronExpression;
import com.xlz.util.DateUtil;

public class CronExpressionTest {

	public static void main(String[] args) throws Exception {
		for(int i = 0 ;i < 10 ;i++){
			String cronExpression = "*/1 1,14,17,10,59 * * * ?";
			CronExpression cexpStart = new CronExpression(cronExpression);
			Date current = new Date( );
			Date nextStartTime = cexpStart.getNextValidTimeAfter(current);
			System.out.println(DateUtil.parseDateToStr(nextStartTime, DateUtil.DATE_TIME_FORMAT_YYYY_MM_DD_HH_MI_SS));
		}
		
		/*String number = "1a";
		Long.valueOf(number);*/
		
		CountDownLatch countDownLatch = new CountDownLatch(20);
		for(int i = 0;i < 20;i++){
			countDownLatch.countDown();
		}
		countDownLatch.await();
		System.out.println("============结束了=============");
		
	}
}
