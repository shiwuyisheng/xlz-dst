package com.xlz.domain;

/**
 * 应用任务实体类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class AppTask extends BaseDomain  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String taskName;
	private String appNo;
	private String dealBean;
	private String cronExpression;
	private String taskParam;
	private Long fetchDataNumber;
	private Integer taskGroupCount;
	private Integer singleTaskThreadCount;
	private String shardItem;
	//取出是判断如果为null则设置为127.0.0.1
	private String bindIp;
	private Integer active;
	private String createUser;
	private String updateUser;
	private String executeMethod;
	private volatile Integer readiness;
	private Long taskBacklogData;
	private Long deadTime;

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getDealBean() {
		return dealBean;
	}

	public void setDealBean(String dealBean) {
		this.dealBean = dealBean;
	}

	public String getTaskParam() {
		return taskParam;
	}

	public void setTaskParam(String taskParam) {
		this.taskParam = taskParam;
	}

	public Long getFetchDataNumber() {
		return fetchDataNumber;
	}

	public void setFetchDataNumber(Long fetchDataNumber) {
		this.fetchDataNumber = fetchDataNumber;
	}

	public Integer getSingleTaskThreadCount() {
		return singleTaskThreadCount;
	}

	public void setSingleTaskThreadCount(Integer singleTaskThreadCount) {
		this.singleTaskThreadCount = singleTaskThreadCount;
	}

	public String getShardItem() {
		return shardItem;
	}

	public void setShardItem(String shardItem) {
		this.shardItem = shardItem;
	}

	public String getBindIp() {
		return bindIp;
	}

	public void setBindIp(String bindIp) {
		this.bindIp = bindIp;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getReadiness() {
		return readiness;
	}

	public void setReadiness(Integer readiness) {
		this.readiness = readiness;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Long getTaskBacklogData() {
		return taskBacklogData;
	}

	public void setTaskBacklogData(Long taskBacklogData) {
		this.taskBacklogData = taskBacklogData;
	}

	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}

	public String getExecuteMethod() {
		return executeMethod;
	}

	public void setExecuteMethod(String executeMethod) {
		this.executeMethod = executeMethod;
	}

	public Integer getTaskGroupCount() {
		return taskGroupCount;
	}

	public void setTaskGroupCount(Integer taskGroupCount) {
		this.taskGroupCount = taskGroupCount;
	}

	public Long getDeadTime() {
		return deadTime;
	}

	public void setDeadTime(Long deadTime) {
		this.deadTime = deadTime;
	}

	@Override
	public String toString() {
		return "AppTask [taskName=" + taskName + ", appNo=" + appNo + ", dealBean=" + dealBean
				+ ", cronExpression=" + cronExpression + ", taskParam=" + taskParam + ", fetchDataNumber="
				+ fetchDataNumber + ", taskGroupCount=" + taskGroupCount + ", singleTaskThreadCount="
				+ singleTaskThreadCount + ", shardItem=" + shardItem + ", bindIp=" + bindIp + ", active=" + active
				+ ", createUser=" + createUser + ", updateUser=" + updateUser + ", executeMethod=" + executeMethod
				+ ", readiness=" + readiness + ", taskBacklogData=" + taskBacklogData + ", deadTime=" + deadTime + "]";
	}

}
