package com.xlz.domain;

/**
 * 执行统计映射实体类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class StatisticExecute extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 应用编号 */
	private String appNo;  
	/** 任务id */
	private Long taskId;  
	/** 注册的实例id */
	private String registerInstanceId;  
	/** 注册的任务组 */
	private String registerWorker;  
	/** 处理数据时间（最近一次心跳上传） */
	private Long executeDataTime;  
	/** 执行成功的记录总条数 */
	private Long executeSuccessTotal;  
	/** 执行失败的记录总条数 */
	private Long executeFailTotal;  
  	
	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getRegisterInstanceId() {
		return registerInstanceId;
	}

	public void setRegisterInstanceId(String registerInstanceId) {
		this.registerInstanceId = registerInstanceId;
	}
	public String getRegisterWorker() {
		return registerWorker;
	}

	public void setRegisterWorker(String registerWorker) {
		this.registerWorker = registerWorker;
	}

	public Long getExecuteDataTime() {
		return executeDataTime;
	}

	public void setExecuteDataTime(Long executeDataTime) {
		this.executeDataTime = executeDataTime;
	}
	public Long getExecuteSuccessTotal() {
		return executeSuccessTotal;
	}

	public void setExecuteSuccessTotal(Long executeSuccessTotal) {
		this.executeSuccessTotal = executeSuccessTotal;
	}
	public Long getExecuteFailTotal() {
		return executeFailTotal;
	}

	public void setExecuteFailTotal(Long executeFailTotal) {
		this.executeFailTotal = executeFailTotal;
	}

	@Override
	public String toString() {
		return "StatisticExecute [appNo=" + appNo + ", taskId=" + taskId + ", registerInstanceId=" + registerInstanceId
				+ ", registerWorker=" + registerWorker + ", executeDataTime=" + executeDataTime
				+ ", executeSuccessTotal=" + executeSuccessTotal + ", executeFailTotal=" + executeFailTotal + "]";
	}

}