package com.xlz.domain;

import java.util.Date;

/**
 * 应用信息实体类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class App extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appNo;
	private String appName;
	private Long heartBeatRate;
	private Integer deadHeartCount;
	private Integer active;
	private String createUser;
	private String updateUser;
	private Date currentTime;

	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public Long getHeartBeatRate() {
		return heartBeatRate;
	}

	public void setHeartBeatRate(Long heartBeatRate) {
		this.heartBeatRate = heartBeatRate;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}

	public Integer getDeadHeartCount() {
		return deadHeartCount;
	}

	public void setDeadHeartCount(Integer deadHeartCount) {
		this.deadHeartCount = deadHeartCount;
	}

	@Override
	public String toString() {
		return "App [appNo=" + appNo + ", appName=" + appName + ", heartBeatRate=" + heartBeatRate
				+ ", deadHeartCount=" + deadHeartCount + ", active=" + active + ", createUser=" + createUser
				+ ", updateUser=" + updateUser + ", currentTime=" + currentTime + "]";
	}

}
