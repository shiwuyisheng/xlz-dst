package com.xlz.domain;

/**
 * 分片信息解析映射实体类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class ShardItem {
	
	private Integer itemId;
	private String itemParam;
	
	public ShardItem(Integer itemId, String itemParam) {
		super();
		this.itemId = itemId;
		this.itemParam = itemParam;
	}
	
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public String getItemParam() {
		return itemParam;
	}
	public void setItemParam(String itemParam) {
		this.itemParam = itemParam;
	}
	public static ShardItem build(String taskItem){
		String params[] = taskItem.split(":");
		return new ShardItem(Integer.valueOf(params[0]),params[1]);
	}

	@Override
	public String toString() {
		return "ShardItem [itemId=" + itemId + ", itemParam=" + itemParam + "]";
	}
	
}
