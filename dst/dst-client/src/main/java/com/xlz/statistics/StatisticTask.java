package com.xlz.statistics;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xlz.manager.DefaultDstManager;
import com.xlz.worker.Worker;

/**
 * 刷新本地task
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class StatisticTask implements Runnable {
 
	protected final Logger LOG = LoggerFactory.getLogger(getClass());
	
	private DefaultDstManager scheduleManager;
	
	public StatisticTask(DefaultDstManager scheduleManager){
		this.scheduleManager = scheduleManager;
	}
	
    @Override
    public void run() {
    	//LOG.info("进行数据统计开始。。。");
    	try {
			Map<Long, Worker> taskWorkerPool = scheduleManager.getWorkerPool();
			for(Map.Entry<Long, Worker> entry : taskWorkerPool.entrySet()){
				Worker worker = entry.getValue();
				worker.flushStatistic();
			}
		} catch (Exception e) {
			LOG.error("持久化统计信息异常：",e);
		}
    	//LOG.info("进行数据统计结束。。。");
    }

}
