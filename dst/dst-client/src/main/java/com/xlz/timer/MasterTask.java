package com.xlz.timer;

import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xlz.manager.DefaultDstManager;

/**
 * 刷新本地task
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class MasterTask extends TimerTask {
 
	protected final Logger LOG = LoggerFactory.getLogger(getClass());
	
	private DefaultDstManager scheduleManager;
	private Timer refreshTasks;
	
	public MasterTask(DefaultDstManager scheduleManager,Timer refreshTasks){
		this.scheduleManager = scheduleManager;
		this.refreshTasks = refreshTasks;
	}
	
    @Override
    public void run() {
    	Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
    	long start = System.currentTimeMillis();
    	try {
    		this.cancel();
			scheduleManager.refreshTasks();
		} catch (Exception e) {
			LOG.error("刷新任务列表异常，当前引用id：{}",scheduleManager.getContext().getInstanceId(),e);
		}
    	long end = System.currentTimeMillis();
    	if(end-start > scheduleManager.getContext().getHeartBeatRate() * 1000)
    		LOG.error("刷新任务列表成功，耗时{}ms,超过了一个心跳时长",(end-start));
		refreshTasks.schedule(new MasterTask(scheduleManager,refreshTasks), 3000);
    }

}
