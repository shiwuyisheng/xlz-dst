package com.xlz.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.xlz.domain.StatisticExecute;

/**
 * 执行统计CRUD基础工具类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class StatisticExecuteService extends BaseJdbcService<StatisticExecute>{
	
	public StatisticExecuteService(DataSource dataSource) {
		super(dataSource);
	}

	public int insert(StatisticExecute entity)  {
		int count = 0;
		Connection connection = null ;
		PreparedStatement pstmt = null;
		String sql = "INSERT into statistic_execute("
				+    "app_no,task_id,register_instance_id,register_worker,execute_data_time,execute_success_total,execute_fail_total,create_time"
				+ 	 ")VALUES(?,?,?,?,?,?,?,now())";
		try {
			connection = getDataSource().getConnection();
			pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, entity.getAppNo());
			pstmt.setLong(2, entity.getTaskId());
			pstmt.setString(3, entity.getRegisterInstanceId());
			pstmt.setString(4, entity.getRegisterWorker());
			pstmt.setLong(5, entity.getExecuteDataTime());
			pstmt.setLong(6, entity.getExecuteSuccessTotal());
			pstmt.setLong(7, entity.getExecuteFailTotal());
			count = pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			if(connection != null)
				try {
					connection.rollback();
				} catch (SQLException e1) {
					LOG.error("执行sql回滚时存在异常",sql,e1);
				}
			LOG.error("执行sql存在异常",sql,e);
		}finally{
			closeResources( connection,  pstmt);
		}
		return count;
	}

	@Override
	public String getNamespace() {
		return "com.xlz.pojo.StatisticSelectService.";
	}
}
