package com.xlz.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.xlz.domain.StatisticSelect;

/**
 * 加载统计CRUD基础工具类.
 * @author 张蕾蕾
 * @date 2018 03 24
 */
public class StatisticSelectService extends BaseJdbcService<StatisticSelect>{
	
	public StatisticSelectService(DataSource dataSource) {
		super(dataSource);
	}

	public int [] insertBatch(String registerInstanceId, List<StatisticSelect> list)  {
		int[] count = null;
		Connection connection = null ;
		PreparedStatement pstmt = null;
		String sql = "INSERT into statistic_select("
				+    "app_no,task_id,register_instance_id,register_worker,load_data_time,load_data_total,load_data_flag,error_content,create_time"
				+ 	 ")VALUES(?,?,?,?,?,?,?,?,now())";
		try {
			connection = getDataSource().getConnection();
			pstmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			for (StatisticSelect entity : list) {
				pstmt.setString(1, entity.getAppNo());
				pstmt.setLong(2, entity.getTaskId());
				pstmt.setString(3, registerInstanceId);
				pstmt.setString(4, entity.getRegisterWorker());
				pstmt.setLong(5, entity.getLoadDataTime());
				pstmt.setLong(6, entity.getLoadDataTotal());
				pstmt.setInt(7, entity.getLoadDataFlag());
				pstmt.setString(8, entity.getErrorContent());
				pstmt.addBatch();
			}
			count = pstmt.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			if(connection != null)
				try {
					connection.rollback();
				} catch (SQLException e1) {
					LOG.error("执行sql回滚时存在异常",sql,e1);
				}
			LOG.error("执行sql存在异常",sql,e);
		}finally{
			closeResources( connection,  pstmt);
		}
		return count;
	}

	@Override
	public String getNamespace() {
		return "com.xlz.pojo.StatisticSelectService.";
	}
}
